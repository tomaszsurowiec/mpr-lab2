class Role{
	private String role;
	private Permissions permissions;
	
	public String getRole(){
		return role;
	}
	
	public void setRole(role){
		this.role = role;
	}
	
	public String getPermissions(){
		return permissions;
	}
	
}